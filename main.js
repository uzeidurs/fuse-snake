var Observable = require('FuseJS/Observable');

var columns = 18;
var rows = 32;
var showHints = Observable(true);

var score = Observable(0);
var snake = Observable();
var blocks = [];
var food = {'x':0,'y':0};
var direction = 'right';

function create_canvas() {
	blocks = [];
	for (var y = 0; y < rows; y++) {
		for (var x = 0; x < columns; x++) {
			blocks.push(new Square(x, y));
		}
	}
};

function Square(x,y) {
	this.x = x;
	this.y = y;
	this.absX = x*10;
	this.absY = y*10;
	this.color = Observable('#eee');
};

function init() {
	score.value = 0;
	direction = 'right';
	create_snake();
	create_food();
};

function set_loop() {
	if (typeof game_loop != 'undefined') clearInterval(game_loop);
	var speed = 300;
	var accel = snake.length - 5;
	speed = speed - (accel * 10);
	if (speed < 30) speed = 30;
	game_loop = setInterval(move_snake, speed);
};

function create_snake() {
	snake.forEach(function(snakeBlock) {
		setBlockType(snakeBlock.x, snakeBlock.y, 'empty');
	});
	snake.clear();
	for (var i = 4; i >= 0; i--) {
		setBlockType(i, 0, 'snake');
		snake.add({'x':i,'y':0});
	}
};

function create_food()
{
	var x = Math.floor((Math.random() * columns));
	var y = Math.floor((Math.random() * rows));
	if (check_collision(x,y)) {
		create_food();
	} else {
		setBlockType(food.x, food.y, 'empty');
		food.x = x;
		food.y = y;
		setBlockType(food.x, food.y, 'food');
		set_loop();
	}
};

function setBlockType(x, y, type) {
	switch (type) {
		case 'empty':
			blocks[getBlockIndex(x,y)].color.value = '#eee';
		break;
		case 'snake':
			blocks[getBlockIndex(x,y)].color.value = '#abc';
		break;
		case 'food':
			blocks[getBlockIndex(x,y)].color.value = '#cba';
		break;
	}
};

function getBlockIndex(x, y) {
	return (x + (y * columns));
};

function move_snake() {
	var nx = snake.getAt(0).x;
	var ny = snake.getAt(0).y;
	switch (direction) {
		case 'right':
			nx++;
		break;
		case 'left':
			nx--;
		break;
		case 'up':
			ny--;
		break;
		case 'down':
			ny++;
		break;
	}

	if (nx == -1 || nx == columns || ny == -1 || ny == rows || check_collision(nx, ny)) {
		init();
		return;
	}

	if (nx == food.x && ny == food.y) {
		score.value = score.value + 1;
		create_food();
	} else {
		var tail = snake.getAt(snake.length-1);
		setBlockType(tail.x, tail.y, 'empty');
		snake.removeAt(snake.length-1);
	}
	
	var tmp = snake.toArray();
	tmp.unshift({'x':nx,'y':ny});
	setBlockType(nx, ny, 'snake');
	snake.replaceAll(tmp);
};

function check_collision(x, y) {
	var collided = false;
	snake.forEach(function(snakeBlock, index) {
		if (snakeBlock.x == x && snakeBlock.y == y && index != snake.length-1) collided = true;
	});
	return collided;
}

function switchDirection(args) {
	showHints.value = false;
	switch (args.sender) {
		case 'GoUp':
			if (direction != 'down') direction = 'up';
		break;
		case 'GoDown':
			if (direction != 'up') direction = 'down';
		break;
		case 'GoRight':
			if (direction != 'left') direction = 'right';
		break;
		case 'GoLeft':
			if (direction != 'right') direction = 'left';
		break;
	}
};

create_canvas();
init();

module.exports = {
	'blocks': blocks,
	'switchDirection': switchDirection,
	'showHints': showHints,
	'score': score.map(function(val) { return 'Score: ' + val; })
};
